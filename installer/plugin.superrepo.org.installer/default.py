# -*- coding: cp1252 -*-

__script__       = "Unknown"
__plugin__       = "plugin-superrepo"
__addonID__      = "plugin.superrepo.org.installer"
__author__       = "Bart Otten (http://www.superrepo.org)"
__url__          = "http://www.superrepo.org"
__credits__      = "SuperRepo"
__platform__     = "xbmc media center"
__date__         = "30-11-2012"
__version__      = "0.0.3"


import os
import fileinput

# xbmc modules
import xbmc
import xbmcgui
from xbmcaddon import Addon

SOURCES_DATA  = xbmc.translatePath( "special://masterprofile/" 'sources.xml' )

def startPlugin():
    
  with open(SOURCES_DATA, 'a+') as f:
    for line in f:
         if 'use.superrepo.org' in line:
            xbmc.executebuiltin('XBMC.ActivateWindow(addonbrowser)')
            sys.exit()
    else: # not found
      for line in fileinput.input(SOURCES_DATA, inplace = 1):
	print line.replace("</files>", '	<source>\n            <name>SuperRepo.org Virtual Disk</name>\n            <path pathversion="1">http://use.superrepo.org/Frodo/</path>\n        </source>\n   </files>'),

      dialog = xbmcgui.Dialog()
      if dialog.yesno('Installation complete','              SuperRepo has been added to the sources','                             Please restart your XBMC'):
	xbmc.executebuiltin('XBMC.ActivateWindow(shutdownmenu)')
      else:
	  sys.exit()
  
startPlugin()